#ifndef __ISL29125_H_
#define __ISL29125_H_

uint8_t isl29125_init();

#define ISL29125_INIT_OK              ESP_OK
#define ISL29125_INIT_FAIL            0x01

#define ISL29125_GREEN                0x01
#define ISL29125_RED                  0x02
#define ISL29125_BLUE                 0x03

#define ISL29125_I2C_ADDR             0b1000100
#define ISL29125_REG_READ_ID          0x00
#define ISL29125_REG_READ_ID_DEFAULT  0x7D

#define ISL29125_REG_CONFIG_1         0x01
#define ISL29225_REG_CONFIG_2         0x02
#define ISL29325_REG_CONFIG_3         0x03

#define ISL29125_REG_LOW_THRESH_LB    0x04
#define ISL29125_REG_LOW_THRESH_HB    0x05

#define ISL29125_REG_HIGH_THRESH_LB   0x06
#define ISL29125_REG_HIGH_THRESH_HB   0x07

#define ISL29125_REG_STATUS           0x08

#define ISL29125_REG_GREEN_LB         0x09
#define ISL29125_REG_GREEN_HB         0x0a
#define ISL29125_REG_RED_LB           0x0b
#define ISL29125_REG_RED_HB           0x0c
#define ISL29125_REG_BLUE_LB          0x0d
#define ISL29125_REG_BLUE_HB          0x0e

// CONFIGURATION 1 - REGISTERS
#define ISL29125_CONFIG1_MODE0        0x00
#define ISL29125_CONFIG1_MODE1        0x01
#define ISL29126_CONFIG1_MODE2        0x02
#define ISL29125_CONFIG1_RNG          0x03
#define ISL29125_CONFIG1_BITS         0x04
#define ISL29125_CONFIG1_SYNC         0x05

// CONFIGURATION 1 - BITS
#define ISL29125_MODE_PWR_DOWN        0x00
#define ISL29125_MODE_GREEN           0x01
#define ISL29125_MODE_RED             0x02
#define ISL29125_MODE_BLUE            0x03
#define ISL29125_MODE_STANDBY         0x04
#define ISL29125_MODE_RGB             0x05
#define ISL29125_MODE_RG              0x06
#define ISL29125_MODE_GB              0x07

#define ISL29125_RANGE_375            0x00
#define ISL29125_RANGE_10k            0x01

// CONFIGURATION 2 - REGISTERS
#define ISL29125_CONFIG2_ALSCC0       0x00
#define ISL29125_CONFIG2_ALSCC1       0x01
#define ISL29125_CONFIG2_ALSCC2       0x02
#define ISL29125_CONFIG2_ALSCC3       0x03
#define ISL29125_CONFIG2_ALSCC4       0x04
#define ISL29125_CONFIG2_ALSCC5       0x05
#define ISL29125_CONFIG2_IR_COM       0x07



// CONFIGURATION 3 - REGISTERS
#define ISL29125_CONFIG3_INTSEL_LB    0x00
#define ISL29125_CONFIG3_INTSEL_HB    0x01
#define ISL29125_CONFIG3_PRST_LB      0x02
#define ISL29125_CONFIG3_PRST_HB      0x03
#define ISL29125_CONFIG3_CONVEN       0x04

// CONFIGURATION 3 - BITS
// Interrupt color select bits
#define ISL29125_INTERRUPT_NONE       0x00
#define ISL29125_INTERRUPT_GREEN      ISL29125_GREEN
#define ISL29125_INTERRUPT_RED        ISL29125_RED
#define ISL29125_INTERRUPT_BLUE       ISL29125_BLUE

// Interrupt intregration cycle count bits
#define ISL29125_INTEGRATIONS1        0x00
#define ISL29125_INTEGRATIONS2        0x01
#define ISL29125_INTEGRATIONS4        0x02
#define ISL29125_INTEGRATIONS8        0x03

// Interrupt on color converstion enable bits
#define ISL29125_CONVEN_DISABLE       0x00
#define ISL29125_CONVEN_ENABLE        0x01

// STATUS REGISTERS
#define ISL29125_STATUS_RGBTHF        0x00
#define ISL29125_STATUS_CONVENF       0x01
#define ISL29125_STATUS_BOUTF         0x02
#define ISL29125_STATUS_RGBCF_LB      0x04
#define ISL29125_STATUS_RGBCF_HB      0x05

// STATUS BITS
#define ISL29125_CONVEN_NOOP          0x00
#define ISL29125_CONVEN_GREEN         ISL29125_GREEN
#define ISL29125_CONVEN_RED           ISL29125_RED
#define ISL29125_CONVEN_BLUE          ISL29125_BLUE

uint8_t isl29125_read_id();

uint16_t isl29125_read_red();
uint16_t isl29125_read_green();
uint16_t isl29125_read_blue();

#endif

#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_heap_caps.h"
#include "driver/gpio.h"
#include "driver/i2c.h"

#include "esp32-isl29125.h"

#define TAG "ISL29125"

#define _I2C_NUMBER(num) I2C_NUM_##num
#define I2C_NUMBER(num) _I2C_NUMBER(num)

#define DATA_LENGTH 512                  /*!< Data buffer length of test buffer */
#define RW_TEST_LENGTH 128               /*!< Data length for r/w test, [0,DATA_LENGTH] */
#define DELAY_TIME_BETWEEN_ITEMS_MS 1000 /*!< delay time between different test items */

#define ISL29125_SCL_IO CONFIG_ISL29125_SCL               /*!< gpio number for I2C master clock */
#define ISL29125_SDA_IO CONFIG_ISL29125_SDA               /*!< gpio number for I2C master data  */
#define ISL29125_NUM I2C_NUMBER(CONFIG_ISL29125_PORT_NUM) /*!< I2C port number for master dev */
#define ISL29125_FREQ_HZ CONFIG_ISL29125_FREQUENCY        /*!< I2C master clock frequency */
#define ISL29125_TX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */
#define ISL29125_RX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */


static i2c_port_t i2c_port = ISL29125_NUM;

esp_err_t isl29125_write_reg(uint8_t addr, uint8_t data) {
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, (ISL29125_I2C_ADDR << 1) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write(cmd, (uint8_t*)&addr, 1, ACK_CHECK_EN);
  i2c_master_write(cmd, (uint8_t*)&data, 1, ACK_CHECK_EN);
  i2c_master_stop(cmd);
  esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);

  return ret;
}

uint8_t isl29125_read_reg(int8_t data_addr) {
  uint8_t len = 1;
  uint8_t *data = malloc(len);

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);

  if (data_addr != -1) {
    i2c_master_write_byte(cmd, ISL29125_I2C_ADDR << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, data_addr, ACK_CHECK_EN);
    i2c_master_start(cmd);
  }

  i2c_master_write_byte(cmd, ISL29125_I2C_ADDR << 1 | READ_BIT, ACK_CHECK_EN);

  if (len > 1) {
    i2c_master_read(cmd, data, len - 1, ACK_VAL);
  }

  i2c_master_read_byte(cmd, data + len - 1, NACK_VAL);
  i2c_master_stop(cmd);
  i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);

  return data[0];
}

uint16_t isl29125_read_reg16(uint8_t data_addr) {
  uint16_t value = isl29125_read_reg(data_addr);
  value |= (isl29125_read_reg(data_addr + 1) << 8);

  return value;
}

uint8_t isl29125_read_id() {
  return isl29125_read_reg(ISL29125_REG_READ_ID);
}

uint16_t isl29125_read_red() {
  return isl29125_read_reg16(ISL29125_REG_RED_LB);
}

uint16_t isl29125_read_green() {
  return isl29125_read_reg16(ISL29125_REG_GREEN_LB);
}

uint16_t isl29125_read_blue() {
  return isl29125_read_reg16(ISL29125_REG_BLUE_LB);
}

uint8_t isl29125_init() {
  esp_err_t err;

  i2c_config_t conf;
  conf.mode = I2C_MODE_MASTER;
  conf.sda_io_num = ISL29125_SDA_IO;
  conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
  conf.scl_io_num = ISL29125_SCL_IO;
  conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
  conf.master.clk_speed = ISL29125_FREQ_HZ;
  i2c_param_config(i2c_port, &conf);

  err = i2c_driver_install(i2c_port, conf.mode, ISL29125_RX_BUF_DISABLE, ISL29125_TX_BUF_DISABLE, 0);
  ESP_ERROR_CHECK(err);

  uint8_t id = isl29125_read_id();

  ESP_LOGD(TAG, "DEVICE_ID: 0x%02X", id);

  if(id != ISL29125_REG_READ_ID_DEFAULT) {
    ESP_LOGE(TAG, "Got incorrect Device ID");

    return ISL29125_INIT_FAIL;
  }

  isl29125_write_reg(ISL29125_REG_CONFIG_1, ISL29125_MODE_RGB | ISL29125_RANGE_375);

  return ISL29125_INIT_OK;
};
